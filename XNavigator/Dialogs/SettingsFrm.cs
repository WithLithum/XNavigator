﻿namespace XNavigator.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XNavigator.Properties;

public partial class SettingsFrm : Form
{
    public SettingsFrm()
    {
        InitializeComponent();

        var settings = Settings.Default;
        checkShowDotHeaded.Checked = settings.ShowDottedFiles;
        checkShowHidden.Checked = settings.ShowHiddenFiles;
        checkShowSystem.Checked = settings.ShowSystemFiles;
        checkSystemDisplay.Checked = settings.UseSystemFileDisplayName;
        checkAlwaysNuke.Checked = settings.AlwaysNukeFile;
    }

    private void buttonOk_Click(object sender, EventArgs e)
    {
        var settings = Settings.Default;
        settings.ShowDottedFiles = checkShowDotHeaded.Checked;
        settings.ShowHiddenFiles = checkShowHidden.Checked;
        settings.ShowSystemFiles = checkShowSystem.Checked;
        settings.UseSystemFileDisplayName = checkSystemDisplay.Checked;
        settings.AlwaysNukeFile = checkAlwaysNuke.Checked;

        settings.Save();
        Close();
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
        Close();
    }

    private void SettingsFrm_Load(object sender, EventArgs e)
    {

    }
}
