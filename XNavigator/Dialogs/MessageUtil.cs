﻿namespace XNavigator.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

internal static class MessageUtil
{
    internal static void ShowAbout(IWin32Window owner)
    {
        var version = Assembly.GetExecutingAssembly()?.GetName()?.Version?.ToString() ?? "unknown";
        var n = Environment.NewLine;

        var tdp = new TaskDialogPage()
        {
            Caption = Locale.AboutCaption,
            Heading = Locale.AppName,
            Text = $"{string.Format(Locale.AboutLine1, Locale.AppName, version)}{n}{Locale.AboutLine2}",
            Buttons = {TaskDialogButton.OK},
            Icon = TaskDialogIcon.Information
        };

        TaskDialog.ShowDialog(owner, tdp);
    }

    internal static void ShowError(IWin32Window owner, string message)
    {
        var tdp = new TaskDialogPage()
        {
            Caption = Locale.ErrorCaption,
            Text = message,
            Icon = TaskDialogIcon.Error,
            Buttons = { TaskDialogButton.OK },
            AllowMinimize = false,
            DefaultButton = TaskDialogButton.OK,
            SizeToContent = true
        };

        TaskDialog.ShowDialog(owner, tdp);
    }
}
