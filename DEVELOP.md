# Development Information

This document covers the development of this project.

## Branching

* Nothing on the `trunk` branch is deployable nor stable.
* Once release is made, it get pushed to `release-a.x`, with `a` being the major version. Updates for that major version will be pushed to that branch.

## Building

You will need, at bare minimum:

* Microsoft Windows computer
* .NET Desktop SDK

Run the following commands at the directory where you cloned this
repository:

```bat
dotnet restore
dotnet build
```

And it should build without issues except when a terrible mistake is made in one of the commits which
causes a compiler error.

### With Visual Studio

Open the repository with IDE and go to BUILD -> Build Solution.

## Icons

Whenever you are able to, use Visual Studio 2022 Image Library icons.