# XNavigator

XNavigator is a simple file manager written in C#.

## Features

- [x] File listing and browsing
- [ ] File properties viewing
- [x] Open files within manager
- [ ] Windows Context menu support
- [ ] Open large folders
- [ ] Go back/forward

## Contributing

Pull requests are welcome. If you make any "larger" changes, please open an issue before you make any pull requests regarding the change.

## Licence

This project is licensed under [GNU GPL version 3](LICENSE) or any later version of your choice.