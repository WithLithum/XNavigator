﻿namespace XNavigator.Util;

using IWshRuntimeLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Vanara.PInvoke;

using File = System.IO.File;

public static class FileUtil
{
    internal static readonly ReadOnlyDictionary<string, int> _primitiveIndices = new(new Dictionary<string, int>()
    {
        {".txt", 1},
        {".log", 1},
        {".node_repl_history", 1},
        {"", 2},
        {".xaml", 3},
        {".xml", 3},
        {".ini", 4},
        {".inf", 4},
        {".config", 4},
        {".properties", 4},
        {".cfg", 4},
        {".npmrc", 4},
        {".yarnrc", 4},
        {".bashrc", 4},
        {".bin", 6},
        {".dat", 6},
        {".sys", 6},
        {".bat", 7},
        {".cmd", 7},
        {".dll", 8},
        {".so", 8},
        {".jpg", 9},
        {".png", 9},
        {".jpeg", 9},
        {".jiff", 9},
        {".gif", 9},
        {".tiff", 9},
        {".dds", 9},
        {".raw", 9},
        {".svg", 9},
        {".ico", 9},
        {".csproj", 10},
        {".cs", 11},
        {".json", 12},
        {".jsonc", 12}
    });

    public static string GetShortcutTarget(string file)
    {
        if (!File.Exists(file))
        {
            throw new FileNotFoundException(null, file);
        }

        var shell = new IWshRuntimeLibrary.WshShell();
        var shortcut = (WshShortcut)shell.CreateShortcut(file);

        return shortcut.TargetPath;
    }

    public static int? GetPrimitiveIndex(string extension)
    {
        var x = extension.ToLower();
        if (!_primitiveIndices.ContainsKey(x))
        {
            return null;
        }

        return _primitiveIndices[x];
    }

    public static bool Recycle(string path)
    {
        var operation = new Shell32.SHFILEOPSTRUCT
        {
            fFlags = Shell32.FILEOP_FLAGS.FOF_ALLOWUNDO | Shell32.FILEOP_FLAGS.FOF_WANTNUKEWARNING,
            pFrom = path,
            wFunc = Shell32.ShellFileOperation.FO_DELETE
        };

        return Shell32.SHFileOperation(ref operation) != 0;
    }

    public static string GetFileTypeName(string path)
    {
        return GetFileInfo(path, Shell32.SHGFI.SHGFI_TYPENAME).szTypeName;
    }

    public static Shell32.SHFILEINFO GetFileInfo(string path, Shell32.SHGFI infoTypes)
    {
        Shell32.SHFILEINFO psfi = new();
        var pidl = new Shell32.PIDL(path);
        Shell32.SHGetFileInfo(pidl, FileAttributes.Normal, ref psfi, (int)pidl.Size, infoTypes | Shell32.SHGFI.SHGFI_PIDL);

        return psfi;
    }

    [DllImport("Sfc.dll")]
    private static extern bool SfcIsFileProtected(IntPtr RpcHandle, [MarshalAs(UnmanagedType.LPWStr)] string ProtFileName);

    public static Icon? GetFileIcon(string path)
    {
        Shell32.SHFILEINFO psfi = new();
        var pidl = new Shell32.PIDL(path);
        var info = Shell32.SHGetFileInfo(pidl, FileAttributes.Normal, ref psfi, (int)pidl.Size, Shell32.SHGFI.SHGFI_ICON | Shell32.SHGFI.SHGFI_SMALLICON | Shell32.SHGFI.SHGFI_PIDL);

        if ((int)info == 0)
        {
            return null;
        }

        return Icon.FromHandle(psfi.hIcon.DangerousGetHandle());
    }

    public static bool IsFileCriticalSystem(string path)
    {
        return SfcIsFileProtected(IntPtr.Zero, path);
    }

    public static void ShellExecute(IWin32Window window, string path)
    {
        Shell32.ShellExecute(window.Handle, lpFile: path, nShowCmd: ShowWindowCommand.SW_NORMAL);
    }

    public static bool TryGetLocalisedName(string path, out string? value)
    {
        var builder = new StringBuilder();
        var result = Shell32.SHGetLocalizedName(path, builder, 0, out _);

        if (!result.Succeeded)
        {
            value = null;
            return false;
        }

        value = builder.ToString();
        return true;
    }
}
