﻿namespace XNavigator.Dialogs;

partial class SettingsFrm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            this.checkShowHidden = new System.Windows.Forms.CheckBox();
            this.checkShowDotHeaded = new System.Windows.Forms.CheckBox();
            this.labelNote = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkShowSystem = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkSystemDisplay = new System.Windows.Forms.CheckBox();
            this.checkAlwaysNuke = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // checkShowHidden
            // 
            this.checkShowHidden.AutoSize = true;
            this.checkShowHidden.Location = new System.Drawing.Point(12, 12);
            this.checkShowHidden.Name = "checkShowHidden";
            this.checkShowHidden.Size = new System.Drawing.Size(165, 24);
            this.checkShowHidden.TabIndex = 0;
            this.checkShowHidden.Text = "Show Hidden Files";
            this.checkShowHidden.UseVisualStyleBackColor = true;
            // 
            // checkShowDotHeaded
            // 
            this.checkShowDotHeaded.AutoSize = true;
            this.checkShowDotHeaded.Location = new System.Drawing.Point(12, 42);
            this.checkShowDotHeaded.Name = "checkShowDotHeaded";
            this.checkShowDotHeaded.Size = new System.Drawing.Size(448, 24);
            this.checkShowDotHeaded.TabIndex = 1;
            this.checkShowDotHeaded.Text = "Show Files and Directories with name prefixed by a dot (.)";
            this.checkShowDotHeaded.UseVisualStyleBackColor = true;
            // 
            // labelNote
            // 
            this.labelNote.AutoSize = true;
            this.labelNote.Location = new System.Drawing.Point(12, 183);
            this.labelNote.Name = "labelNote";
            this.labelNote.Size = new System.Drawing.Size(416, 20);
            this.labelNote.TabIndex = 2;
            this.labelNote.Text = "Note: Settings may not apply until you refresh the page.";
            // 
            // buttonOk
            // 
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(500, 290);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(94, 29);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(400, 290);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(94, 29);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // checkShowSystem
            // 
            this.checkShowSystem.AutoSize = true;
            this.checkShowSystem.Location = new System.Drawing.Point(12, 72);
            this.checkShowSystem.Name = "checkShowSystem";
            this.checkShowSystem.Size = new System.Drawing.Size(249, 24);
            this.checkShowSystem.TabIndex = 5;
            this.checkShowSystem.Text = "Show \'Important\' System Files";
            this.toolTip1.SetToolTip(this.checkShowSystem, "Some files are regarded as \"Important System Files\" by Windows. By ticking this b" +
        "ox, you may show them in the file view. ");
            this.checkShowSystem.UseVisualStyleBackColor = true;
            // 
            // checkSystemDisplay
            // 
            this.checkSystemDisplay.AutoSize = true;
            this.checkSystemDisplay.Location = new System.Drawing.Point(12, 102);
            this.checkSystemDisplay.Name = "checkSystemDisplay";
            this.checkSystemDisplay.Size = new System.Drawing.Size(347, 24);
            this.checkSystemDisplay.TabIndex = 6;
            this.checkSystemDisplay.Text = "Use File Display Names provided by System";
            this.toolTip1.SetToolTip(this.checkSystemDisplay, "If checked, the file name displayed by the system is used. This kind of names are" +
        " affected by the Folder Options settings,");
            this.checkSystemDisplay.UseVisualStyleBackColor = true;
            // 
            // checkAlwaysNuke
            // 
            this.checkAlwaysNuke.AutoSize = true;
            this.checkAlwaysNuke.Location = new System.Drawing.Point(12, 132);
            this.checkAlwaysNuke.Name = "checkAlwaysNuke";
            this.checkAlwaysNuke.Size = new System.Drawing.Size(368, 24);
            this.checkAlwaysNuke.TabIndex = 7;
            this.checkAlwaysNuke.Text = "Always permanently delete rather than recycle";
            this.checkAlwaysNuke.UseVisualStyleBackColor = true;
            // 
            // SettingsFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 331);
            this.Controls.Add(this.checkAlwaysNuke);
            this.Controls.Add(this.checkSystemDisplay);
            this.Controls.Add(this.checkShowSystem);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelNote);
            this.Controls.Add(this.checkShowDotHeaded);
            this.Controls.Add(this.checkShowHidden);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsFrm";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.SettingsFrm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private CheckBox checkShowHidden;
    private CheckBox checkShowDotHeaded;
    private Label labelNote;
    private Button buttonOk;
    private Button buttonCancel;
    private CheckBox checkShowSystem;
    private ToolTip toolTip1;
    private CheckBox checkSystemDisplay;
    private CheckBox checkAlwaysNuke;
}