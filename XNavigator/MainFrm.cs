namespace XNavigator;

using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Text;
using System.Media;
using System.Runtime.InteropServices;
using Vanara.PInvoke;
using XNavigator.Dialogs;
using XNavigator.Properties;
using XNavigator.Util;

public partial class MainFrm : Form
{
    private string? current;
    private string? contextCurrent;
    private readonly string _default;

    private readonly ImageList _images = new();
    private int nIndex;

    public MainFrm()
    {
        InitializeComponent();

        listView1.SmallImageList = _images;
        _images.Images.Add(Resources.FolderClosed);
        _images.Images.Add(Resources.TextFile);
        _images.Images.Add(Resources.BlankFile);
        _images.Images.Add(Resources.XmlFile);
        _images.Images.Add(Resources.ConfigurationFile);
        _images.Images.Add(Resources.User);
        _images.Images.Add(Resources.BinaryFile);
        _images.Images.Add(Resources.Script);
        _images.Images.Add(Resources.DynamicLibrary);
        _images.Images.Add(Resources.Image);
        _images.Images.Add(Resources.CSProjectNode);
        _images.Images.Add(Resources.CSFileNode);
        _images.Images.Add(Resources.JsonFile);

        nIndex = _images.Images.Count;
        _default = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
    }

    public MainFrm(string[] args) : this()
    {
        if (args.Length != 1)
        {
            return;
        }

        var dir = args[0];

        if (!Directory.Exists(args[0]))
        {
            _default = dir;
        }
    }

    private void ClearStatus()
    {
        this.Invoke(() =>
        {
            labelStatus.Text = Locale.StatusReady;
        });
    }

    private void SetStatus(string text)
    {
        this.Invoke(() =>
        {
            labelStatus.Text = text;
        });
    }

    public async Task NavigateTo(string folderPath)
    {
        this.Invoke(() => toolStrip1.Enabled = false);
        // If directory does not exist, prompt and return
        if (!Directory.Exists(folderPath))
        {
            MessageUtil.ShowError(this, String.Format(Locale.NoSuchDirectory, folderPath));
            return;
        }

        SetStatus(Locale.StatusOpeningDirectory);

        // Set some values
        textAddress.Text = folderPath;
        current = folderPath;

        // Clears current items
        listView1.Items.Clear();

        string[] dirs;
        string[] files;

        // If access not authorised, prompt and return
        try
        {
            dirs = Directory.GetDirectories(folderPath);
            files = Directory.GetFiles(folderPath);
        }
        catch (UnauthorizedAccessException)
        {
            MessageUtil.ShowError(this, String.Format(Locale.UnauthorisedAccess, folderPath));
            ClearStatus();
            return;
            // This leaves an empty view
        }

        // Save system settings for quicker access
        var settings = Settings.Default;
        var showHidden = settings.ShowHiddenFiles;
        var showSystem = settings.ShowSystemFiles;
        var systemName = settings.UseSystemFileDisplayName;

        var list = new List<ListViewItem>();

        SetStatus(Locale.StatusIndexingFolders);

        // Quickly loop through directories
        foreach (var dir in dirs)
        {
            var f = new DirectoryInfo(dir);

            // Create items
            var i = new ListViewItem(f.Name, 0)
            {
                Text = f.Name,
                SubItems =
                {
                    Locale.DirectoryType
                },
                Tag = $"Dir|{dir}",

            };

            list.Add(i);
        }

        var stopIcon = true; // Disable the feature
        SetStatus(Locale.StatusIndexingFiles);

        var sw = new Stopwatch();
        foreach (var file in files)
        {
            sw.Start();
            try
            {
                // Gets native and managed file info
                var f = new FileInfo(file);
                var nativeInfo = await Task.Run(() => FileUtil.GetFileInfo(file, Shell32.SHGFI.SHGFI_TYPENAME | Shell32.SHGFI.SHGFI_DISPLAYNAME));

                // If hidden, hide unless box ticked to show
                if (f.Attributes == FileAttributes.Hidden && !showHidden)
                {
                    continue;
                }

                // Get file primitive icon index
                var prim = FileUtil.GetPrimitiveIndex(f.Extension);

                // Checks if it is ntuser
                if (prim == null && (f.Name.StartsWith("ntuser.dat.LOG") || f.Name.StartsWith("NTUSER.DAT{")))
                {
                    prim = 5;
                }

                try
                {
                    // If no primitive icon, get system icon
                    if (prim == null)
                    {
                        // If told to stop using icons, stop
                        if (stopIcon)
                        {
                            prim = 2;
                        }
                        else
                        {
                            var icon = Icon.FromHandle(nativeInfo.hIcon.DangerousGetHandle());

                            if (icon != null)
                            {
                                _images.Images.Add(icon);
                            }

                            prim = nIndex++;
                        }
                    }
                }
                catch (Exception x)
                {
                    stopIcon = true;
                    Debug.WriteLine(x);
                    prim = 2;
                }

                var i = new ListViewItem(f.Name, prim ?? 2)
                {
                    Text = systemName ? nativeInfo.szDisplayName : f.Name,
                    SubItems =
                {
                    nativeInfo.szTypeName ?? f.Extension,
                    f.Length.ToString()
                },
                    Tag = $"Fil|{file}"
                };

                // If system, hide
                if (f.Attributes == FileAttributes.System || FileUtil.IsFileCriticalSystem(file))
                {
                    if (showSystem)
                    {
                        i.BackColor = Color.Red;
                        i.ForeColor = Color.White;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (f.Attributes == FileAttributes.Encrypted)
                {
                    i.ForeColor = Color.Blue;
                }

                list.Add(i);
            }
            catch (Win32Exception wex)
            {
                this.Invoke(() =>
                {
                    MessageUtil.ShowError(this, wex.Message);
                });
            }
            sw.Stop();
            Debug.WriteLine(String.Format("{0}ms", sw.ElapsedMilliseconds));
            sw.Reset();
        }
            

        this.BeginInvoke(() =>
        {
            listView1.Visible = false;
            foreach (var i in list)
            {
                listView1.Items.Add(i);
            }
            listView1.Visible = true;
            toolStrip1.Enabled = true;
        });

        ClearStatus();
    }

    public async Task RefreshView()
    {
        if (string.IsNullOrWhiteSpace(current) || !Directory.Exists(current))
        {
            return;
        }

        await NavigateTo(current);
    }

    private async void MainFrm_Load(object sender, EventArgs e)
    {
        this.Text = Locale.AppName;
        await NavigateTo(_default);
    }

    private async void textBox_KeyUp(object sender, KeyEventArgs e)
    {
        if (e.KeyCode == Keys.Enter)
        {
            if (Directory.Exists(textAddress.Text))
            {
                await NavigateTo(textAddress.Text);
            }
            else
            {
                textAddress.SelectAll();
                SystemSounds.Beep.Play();
            }
        }
    }

    private async void buttonGoUp_Click(object sender, EventArgs e)
    {
        if (current == null)
        {
            return;
        }

        var x = Path.GetDirectoryName(current);

        if (x == null || x == current) return;

        await NavigateTo(x);
    }

    private async void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
        // Raycast to item
        Debug.WriteLine("DoubleClick");
        var hitTest = listView1.HitTest(e.X, e.Y);

        var i = hitTest.Item;
        var t = (string)i.Tag;
        // If item invalid
        if (t == null)
        {
            Debug.WriteLine("INVALID #1");
            return;
        }

        // If item tag is not in a valid format
        var splitted = t.Split('|');
        Debug.WriteLine(t);
        if (splitted.Length != 2)
        {
            Debug.WriteLine("INVALID #2");
            return;
        }

        // Check for types
        switch (splitted[0])
        {
            default:
                Debug.WriteLine("INVALID #3");
                return;

            case "Fil" when Path.GetExtension(splitted[1]) == ".lnk":
                // If file, opens
                if (!File.Exists(splitted[1]))
                {
                    break;
                }
                var target = FileUtil.GetShortcutTarget(splitted[1]);
                if (Directory.Exists(target))
                {
                    await NavigateTo(target);
                }
                else
                {
                    FileUtil.ShellExecute(this, splitted[1]);
                }
                break;
            case "Fil":
                // If file, opens
                if (!File.Exists(splitted[1]))
                {
                    break;
                }

                FileUtil.ShellExecute(this, splitted[1]);
                break;

            case "Dir":
                // If folder, navigates
                if (!Directory.Exists(splitted[1]))
                {
                    break;
                }

                await NavigateTo(splitted[1]);
                break;
        }
    }

    private void openToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(contextCurrent) || !File.Exists(contextCurrent))
        {
            return;
        }

        FileUtil.ShellExecute(this, contextCurrent);
    }

    private async void optionsToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (new SettingsFrm().ShowDialog() == DialogResult.OK)
        {
            await RefreshView();
        }
    }

    private async void refreshToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (current == null)
        {
            return;
        }

        await NavigateTo(current);
    }

    private void ContextMenu(MouseEventArgs e)
    {
        var hitTest = listView1.HitTest(e.X, e.Y);

        var tag = (string)hitTest.Item.Tag;
        Debug.WriteLine(tag);

        var splitted = tag.Split('|');

        if (splitted.Length != 2)
        {
            return;
        }

        Debug.WriteLine(splitted[0]);

        if (splitted[0] == "Fil" && File.Exists(splitted[1]))
        {
            Debug.WriteLine("Context File");
            contextCurrent = splitted[1];
            contextFile.Show(listView1, e.X, e.Y);
        }
    }

    private void listView1_MouseClick(object sender, MouseEventArgs e)
    {
        if (e.Button != MouseButtons.Right)
        {
            return;
        }

        ContextMenu(e);
    }

    private void contextView_Opening(object sender, System.ComponentModel.CancelEventArgs e)
    {
        if (contextFile.Visible)
        {
            e.Cancel = true;
        }
    }

    private async void refreshToolStripMenuItem1_Click(object sender, EventArgs e)
    {
        await RefreshView();
    }

    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
        MessageUtil.ShowAbout(this);
    }

    private async void deleteToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (!File.Exists(contextCurrent))
        {
            return;
        }

        if (Settings.Default.AlwaysNukeFile)
        {
            File.Delete(contextCurrent);
        }
        else
        {
            FileUtil.Recycle(contextCurrent);
        }

        await RefreshView();
    }

    private async void itemNewFolder_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(current) || !Directory.Exists(current))
        {
            return;
        }

        var dialog = new InputDialog(Locale.DialogCreateDir, Locale.DialogCreateDirMessage);

        if (dialog.ShowDialog() != DialogResult.OK)
        {
            return;
        }

        var chars = Path.GetInvalidFileNameChars();

        foreach (var c in chars)
        {
            Debug.WriteLine($"Verify {c}");
            foreach (var i in dialog.Input)
            {
                if (i == c)
                {
                    MessageUtil.ShowError(this, Locale.IllegalDirectoryName);
                    return;
                }
            }
        }

        var actual = Path.Combine(current, dialog.Input);
        Directory.CreateDirectory(actual);
        await RefreshView();
    }

    private void openCommandPromptHereToolStripMenuItem_Click(object sender, EventArgs e)
    {
        if (!Directory.Exists(current))
        {
            return;
        }

        Process.Start(new ProcessStartInfo("cmd")
        {
            WorkingDirectory = current
        });
    }
}
