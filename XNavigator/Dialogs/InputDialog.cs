﻿namespace XNavigator.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

public partial class InputDialog : Form
{
    public InputDialog(string title, string message)
    {
        InitializeComponent();
        this.Text = title;
        labelText.Text = message;
    }

    public string Input { get => textBox1.Text; set => textBox1.Text = value; }

    private void buttonOk_Click(object sender, EventArgs e)
    {
        Close();
    }
}
